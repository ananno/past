# A - T or T

Time Limit: 2 sec / Memory Limit: 1024 MB

__Score__ : 100 points

## Problem Statement
$N$ of us are going on a trip, by train or taxi.

The train will cost each of us $$ yen (the currency of Japan).

The taxi will cost us a total of $B$ yen.

How much is our minimum total travel expense?

## Constraints
All values in input are integers.
* $1 \le N \le 20$
* $1 \le A \le 50$
* $1 \le B \le 50$

## Input
Input is given from Standard Input in the following format:
```
N A B
```
## Output
Print an integer representing the minimum total travel expense.

### Sample Input 1 
```
4 2 9
```
### Sample Output 1 
```
8
```
The train will cost us $4×2=8$ yen, and the taxi will cost us $9$ yen, so the minimum total travel expense is $8$ yen.

### Sample Input 2 
```
4 2 7
```
### Sample Output 2 
```
7
```
### Sample Input 3 
```
4 2 8
```
### Sample Output 3 
```
8
```