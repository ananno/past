N, T = map(int, input().split())

c_s = []

for _ in range(N):
    c, t = map(int, input().split())
    if t <= T:
        c_s.append(c)

print(min(c_s) if len(c_s) > 0 else 'TLE')
