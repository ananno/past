N, K = map(int, input().split())
h_s = map(int, input().split())

allowed = [h for h in h_s if h>= K]

print(len(allowed))