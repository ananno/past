N, M, Q = map(int, input().split())

Ns = dict([(n, []) for n in range(N)])
Ms = dict([(m, N) for m in range(M)])

outputs = []

for i in range(Q):
    s = list(map(int, input().split()))

    if s[0] == 1:
        n = s[1] - 1
        outputs.append(sum([Ms[i] for i in Ns[n]]))
    else:
        n, m = s[1]-1, s[2]-1
        Ms[m] -= 1 
        Ns[n] += [m]
        
for output in outputs:
    print(output)
