N, M, Q = map(int, input().split())

class Vertex:
    def __init__(self, name):
        self.name = name
        self.color = 0
    
    def __str__(self):
        return self.name


class Edge:
    def __init__(self, vertex1, vertex2):
        self.vertex1 = vertex1
        self.vertex2 = vertex2

    def has_vertex(self, vertex):
        return True if self.vertex1 == vertex or self.vertex2 == vertex else False

    def __str__(self):
        return "%s <-> %s" % (self.vertex1, self.vertex2)

vertices = dict([(i+1, Vertex(i+1)) for i in range(N)])
edges = []
for _ in range(M):
    v1, v2 = map(int, input().split())
    edges += [Edge(vertices[v1], vertices[v2])]

colors = map(int, input().split())
for i, color in enumerate(colors):
    vertices[i+1].color = color

outputs = []
for _ in range(Q):
    s = list(map(int, input().split()))
    if s[0] == 1:
        vertex= vertices[s[1]]
        color = vertex.color
        outputs.append(color)
        for edge in edges:
            if edge.has_vertex(vertex):
                edge.vertex1.color = color
                edge.vertex2.color = color
    else:
        x, y = s[1], s[2]
        vertex = vertices[x]
        color = vertex.color
        outputs.append(color)
        vertex.color = y

for output in outputs:
    print(output)
