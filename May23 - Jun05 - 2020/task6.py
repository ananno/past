import numpy as np

N = int(input())

grid = np.zeros(shape=(N, N), dtype=np.int)
for i in range(N):
    a = input()
    for j in range(N):
        code = ord(a[j])
        grid[i, j] = code

outputs = []


def check_palindrome(sections):
    for i in range(N):
        sec = sections[i]
        size = sec.shape[0]
        mid, rem = divmod(size, 2)
        if rem == 0:
            sec1, sec2 = np.split(sec, 2)
        else:
            sec1, _, sec2 = np.split(sec, [mid, mid+1, ])
        
        sec2 = sec2[::-1]
        if np.equal(sec1, sec2).all():
            sec = list(map(chr, sec.tolist()))
            outputs.append("".join(sec))

columns = np.squeeze(np.split(grid, N, axis=-1))
check_palindrome(columns)
rows = np.squeeze(np.split(grid, N, axis=0))
check_palindrome(rows)

if len(outputs) == 0:
    print(-1)
else:
    print([output for output in outputs])