from decimal import Decimal

A, R, N = map(Decimal, input().split())
limit = 1E9

A = A * (R ** (N-1))
if A > limit:
    A = 'large'

print(A)