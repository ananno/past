import numpy as np

N = int(input())
rows = 5
cols = (4*N) + 1
result_dict = {}


def process_inputs(input_array=None):
    reset = False if input_array else True
    if reset:
        r, c = 5, 4*10+1    # preparing digits
        total_digits = 10
    else:
        r, c = rows, cols
        total_digits = N
    matrix = np.zeros(shape=[r, c])
    if reset:
        input_array = [".###..#..###.###.#.#.###.###.###.###.###.",
                       ".#.#.##....#...#.#.#.#...#.....#.#.#.#.#.",
                       ".#.#..#..###.###.###.###.###...#.###.###.",
                       ".#.#..#..#.....#...#...#.#.#...#.#.#...#.",
                       ".###.###.###.###...#.###.###...#.###.###."]
    for i in range(r):
        line = input_array[i]
        for j in range(c):
            if line[j] == "#":
                matrix[i, j] = 1
    
    sections = []
    for i in range(0, c, 4):
        sections += [i]

    digits = np.split(matrix, sections, axis=-1)[1:total_digits+1]
    if reset:
        for i, digit in enumerate(digits):
            key = list(map(str, list(map(int, list(digit.reshape([-1]))))))
            key = "".join(key)
            result_dict[key] = i

    return digits


def check_digits(digits):
    result = ""
    for digit in digits:
        key = list(map(str, list(map(int, list(digit.reshape([-1]))))))
        key = "".join(key)
        num = result_dict[key]
        result += str(num)

    return result

process_inputs()

input_canvas = []
for _ in range(rows):
    input_canvas += [input()]

# input_canvas = [".###..#..###.###.#.#.###.###.###.###.###.",
#                        ".#.#.##....#...#.#.#.#...#.....#.#.#.#.#.",
#                        ".#.#..#..###.###.###.###.###...#.###.###.",
#                        ".#.#..#..#.....#...#...#.#.#...#.#.#...#.",
#                        ".###.###.###.###...#.###.###...#.###.###."]
# input_canvas = [".###.###.###.###.###.###.###.###.###.#.#.###.#.#.#.#.#.#.###.###.###.###..#..###.###.###.###.###.#.#.###.###.###.###.",
#                 "...#.#.#...#.#.#.#.#.#...#.#...#.#.#.#.#.#...#.#.#.#.#.#.#.....#.#.#.#.#.##..#.#...#.#.#...#.#...#.#...#.#.....#...#.",
#                 ".###.#.#...#.###.#.#.###.###...#.###.###.###.###.###.###.###...#.###.#.#..#..###...#.###.###.###.###.###.###.###.###.",
#                 ".#...#.#...#...#.#.#.#.#...#...#.#.#...#.#.#...#...#...#.#.#...#...#.#.#..#..#.#...#...#.#...#.#...#.#.....#...#.#...",
#                 ".###.###...#.###.###.###.###...#.###...#.###...#...#...#.###...#.###.###.###.###...#.###.###.###...#.###.###.###.###."]

digits = process_inputs(input_canvas)
results = check_digits(digits)

print(results)
